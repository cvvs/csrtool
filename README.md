csrtool
=======

A simple tool to generate certificate signing requests with subject alternative names.

Usage
=====
csrtool [options] args

```
Usage of csrtool:
  -C/country="": country subject attribute
  -L/locality="": locality/city subject attribute
  -O/org="": organization subject attribute
  -OU/orgunit="": organization unit subject attribute
  -ST/state="": state subject attribute
  -a/algo="rsa": the key algorithm to use, rsa or ecdsa
  -b/rsabits=2048: key size
  -c/csr="request.csr": path to CSR file
  -k/key="key.pem": path to private key
  -verbose=false: verbose output
  args: space separated list of names, the first name is the CN
```

Report any issues, and pull requests are welcome.

TODO
----

* Fix TODOs in code
* Allow user specified SAN delimiter
* Tests and integration with drone or travis
* Allow user to choose from all the curves in the ecdsa package
* Marshalling EC parameters via PEM so we an make ECDSA the default algorthm
* Make sure we don't expose the private EC key by ensuring the curves match
* Allow external sources for random reader so that cloud servers have access to enough entropy?
* Allow subject sources from environment or a config file
* Custom usage function
* Option to print to standard out?
* Option to read key from standard in?


Installation
-----------

```
go get github.com/cvvs/csrtool
```

Contributing
------------

Make sure you run gofmt and go vet before submitting a pull request.
