// Copyright 2015 Chad W. Slater
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file
package main

/*
TODO: Fix TODOs in code
TODO: Tests and integration with drone or travis
TODO: Allow user to choose from all the curves in the ecdsa package
TODO: Marshalling EC parameters via PEM
TODO: Make sure we don't expose the private EC key by ensuring the curves match
TODO: Allow external sources for random reader so that cloud servers have access to enough entropy?
TODO: Allow subject sources from environment
TODO: Custom usage
TODO: Option to print to standard out?
TODO: Option to read key from standard in?
*/

import (
	"bufio"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

const (
	OK             = iota
	ARGUMENT_ERROR = iota
	KEY_ERROR      = iota
	WRITE_ERROR    = iota
	CSR_ERROR      = iota

	MIN_ARGS = 1
)

var (
	// key vars
	privateKeyPath string // path to the file which (will) hold(s) the private key
	csrPath        string // path to the generated CSR
	rsaBits        int    // number of bits in the rsa key
	keyAlgo        string // the type of key to create, rsa or ecdsa
	verbose        bool   // enable verbose output

	// cert vars
	country       string
	stateProvince string
	locality      string
	org           string
	orgUnit       string
)

// Specific errors
type unknownKeyError struct{}

func (e unknownKeyError) Error() string {
	return "Unsupported or Unidentified Key Type"
}

type corruptKeyFileError struct{}

func (e corruptKeyFileError) Error() string {
	return "Key File Corrupted"
}

type notImplementedError struct{}

func (e notImplementedError) Error() string {
	return "Not yet implemented."
}

func init() {

	flag.StringVar(&privateKeyPath, "key", "key.pem", "path to private key")
	flag.StringVar(&privateKeyPath, "k", "key.pem", "path to private key")

	flag.StringVar(&csrPath, "csr", "request.csr", "path to CSR file")
	flag.StringVar(&csrPath, "c", "request.csr", "path to CSR file")

	flag.StringVar(&keyAlgo, "algo", "rsa", "the key algorithm to use, rsa or ecdsa")
	flag.StringVar(&keyAlgo, "a", "rsa", "the key algorithm to use, rsa or ecdsa")

	flag.IntVar(&rsaBits, "rsabits", 2048, "key size")
	flag.IntVar(&rsaBits, "b", 2048, "key size")

	flag.StringVar(&country, "country", "", "country subject attribute")
	flag.StringVar(&country, "C", "", "country subject attribute")

	flag.StringVar(&stateProvince, "state", "", "state subject attribute")
	flag.StringVar(&stateProvince, "ST", "", "state subject attribute")

	flag.StringVar(&locality, "locality", "", "locality/city subject attribute")
	flag.StringVar(&locality, "L", "", "locality/city subject attribute")

	flag.StringVar(&org, "org", "", "organization subject attribute")
	flag.StringVar(&org, "O", "", "organization subject attribute")

	flag.StringVar(&org, "orgUnit", "", "organization unit subject attribute")
	flag.StringVar(&org, "OU", "", "organization unit subject attribute")

	flag.BoolVar(&verbose, "verbose", false, "verbose output") // doesn't do anything

	flag.Parse()

	if flag.NArg() < MIN_ARGS {
		flag.Usage()
		fmt.Println("Error: must provide at least one subject name (eg. foo.example.com)")
		os.Exit(ARGUMENT_ERROR)
	}
}

func generateKey(r io.Reader) (crypto.PrivateKey, error) {
	switch keyAlgo {
	case "ecdsa":
		c := elliptic.P521()
		return ecdsa.GenerateKey(c, r)
	case "rsa":
		return rsa.GenerateKey(r, rsaBits)
	}
	return nil, new(unknownKeyError)
}

func fileExists(fileName string) bool {
	if _, err := os.Stat(fileName); err == nil {
		return true
	}
	return false
}

// A key can be either an RSA key or an ECDSA key.
func readKey(r io.Reader) (crypto.PrivateKey, error) {

	raw, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(raw)
	if block == nil { // see if we could parse the file
		return nil, new(corruptKeyFileError)
	}

	var key crypto.PrivateKey
	if key, err = x509.ParseECPrivateKey(block.Bytes); err == nil {
		return key, nil
	}
	if key, err = x509.ParsePKCS1PrivateKey(block.Bytes); err == nil {
		return key, nil
	}
	return nil, new(unknownKeyError)
}

// Will try to load the key from the provided path. If the file doesn't exist, then we generate a key.
func loadKey(keyPath string) (crypto.PrivateKey, error) {

	var key crypto.PrivateKey

	if fileExists(privateKeyPath) {
		f, err := os.Open(privateKeyPath)
		if err != nil {
			fmt.Printf("Error opening %s: %s\n", privateKeyPath, err)
		}
		r := bufio.NewReader(f)

		if key, err = readKey(r); err != nil {
			return nil, err
		}
		switch key.(type) {
		case *ecdsa.PrivateKey:
			if keyAlgo != "ecdsa" {
				fmt.Printf("ecdsa key loaded, but %s key requested. using existing key\n", keyAlgo)
			}
		case *rsa.PrivateKey:
			if keyAlgo != "rsa" {
				fmt.Printf("rsa key loaded, but %s key requested\n. using existing key", keyAlgo)
			}
		default:
			return nil, new(unknownKeyError)
		}
	} else {
		var err error // TODO: Figure out how to not have to do this. I misunderstand scoping or something.
		key, err = generateKey(rand.Reader)
		if err != nil {
			return nil, err
		}
	}
	return key, nil
}

func saveKey(keyPath string, key crypto.PrivateKey) error {

	if fileExists(keyPath) {
		return nil
	}

	f, err := os.Create(keyPath)
	if err != nil {
		return err
	}
	w := bufio.NewWriter(f)
	defer f.Close()

	var block pem.Block

	switch i := key.(type) {
	case *ecdsa.PrivateKey:
		// TODO: How do we encode and save the parameters of the curve? Do we need to?
		key := key.(*ecdsa.PrivateKey)
		b, err := x509.MarshalECPrivateKey(key)
		if err != nil {
			return err
		}
		block.Type = "EC PARAMETERS"
		err = pem.Encode(w, &block)
		block.Type = "EC PRIVATE KEY"
		block.Bytes = b
	case *rsa.PrivateKey:
		k := x509.MarshalPKCS1PrivateKey(key.(*rsa.PrivateKey))
		block.Type = "RSA PRIVATE KEY"
		block.Bytes = k
	default:
		fmt.Println(i)
		return new(unknownKeyError)
	}

	err = pem.Encode(w, &block)
	if err != nil {
		return err
	}
	return w.Flush()
}

func saveCSR(w io.Writer, csr []byte) error {
	block := pem.Block{Type: "CERTIFICATE REQUEST", Bytes: csr}
	return pem.Encode(w, &block)
}

func newCSR(key crypto.PrivateKey) ([]byte, error) {
	subject := pkix.Name{Country: []string{country}, Province: []string{stateProvince}, Locality: []string{locality},
		Organization: []string{org}, OrganizationalUnit: []string{orgUnit}, CommonName: flag.Args()[0]}

	template := x509.CertificateRequest{Subject: subject, DNSNames: flag.Args()}
	return x509.CreateCertificateRequest(rand.Reader, &template, key)
}

func main() {

	// We need a private key. If we can't get a private key, we need to exit.
	var key crypto.PrivateKey
	// We've already got one!
	key, err := loadKey(privateKeyPath)
	if err != nil {
		fmt.Printf("Error loading key file: %s\n", err)
		os.Exit(KEY_ERROR)
	}

	err = saveKey(privateKeyPath, key)
	if err != nil {
		fmt.Println(err)
		os.Exit(WRITE_ERROR)
	}

	csrBytes, err := newCSR(key)
	if err != nil {
		fmt.Println(err)
		os.Exit(CSR_ERROR)
	}

	f, err := os.Create(csrPath)
	if err != nil {
		fmt.Println("couldn't create", f.Name(), err)
		os.Exit(WRITE_ERROR)
	}

	err = saveCSR(f, csrBytes)
	if err != nil {
		fmt.Println("couldn't save csr", err)
		os.Exit(CSR_ERROR)
	}

	os.Exit(OK)
}
